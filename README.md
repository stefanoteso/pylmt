PyLMT
=====

This package provides a Python implementation of Learning Modulo Theories.

# Requirements

This package requires `numpy` and `OptiMathSAT`, you can find them here:

- [numpy](http://www.numpy.org/)
- [optimathsat](http://optimathsat.disi.unitn.it/), tested with versions 1.3.5 to 1.3.7.

You will also need a slightly modified version of `pystruct`:

 - [modified pystruct](https://bitbucket.org/stefanoteso/pystruct)

Make sure that the `optimathsat` binary is visible from the `$PATH` environment
variable.

# Usage

See the `experiments` directory for some usage examples.

# Common Issues

* Precision issues when converting from ``float``s to rationals.

If your numerical data is stored using ``float``s, make sure to write SMTLIB
problem that can actually tolerate the possible precision loss that occurs
when injecting the ``float``s into the problem file.

In other words, the underlying OMT solver may assume that the rational data
is correct to infinite precision, which may produce intuitively wrong (yet
formally correct!) solutions to the optimization problem, especially when
using equality/inequality constraints.

# Documentation

You can build the in-depth documentation with `sphinx`, just type:
```
    sphinx-build -b html docs/source docs/build
```
The HTML documentation can be found in `docs/build/index.html`.

# References

 - Stefano Teso, Roberto Sebastiani and Andrea Passerini, "*Structured Learning Modulo Theories*", 2014. [link](http://arxiv.org/abs/1405.1675)
 - Roberto Sebastiani and Silvia Tomasi, "*Optimization modulo theories with linear rational costs*", 2015.
 - Roberto Sebastiani and Patrick Trentin, "*OptiMathSAT: a tool for Optimization Modulo Theories*", 2015.
 - Andreas Mueller and Sven Behnke, "*PyStruct - Structured prediction in Python*", JMLR, 2014. [link](http://jmlr.org/papers/v15/mueller14a.html).
 - Ioannis Tsochantaridis *et al.*, "*Large margin methods for structured and interdependent output variables*", 2005.

# Funding

The project is supported by the CARITRO Foundation through grant 2014.0372.
